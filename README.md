# The Little JavaScript Book (Ebook)

Ever dreamed of a JavaScript book focused on the "hard parts" while being beginner friendly? If you answered yes then you will love "The Little JavaScript Book".

* [Read online (free)](manuscript/README.md)
* [Buy on Leanpub](https://leanpub.com/little-javascript/)
* [Download PDF/EPub/Mobi (free)](https://www.valentinog.com/little-javascript)

**NOTE: The book is still work in progress and I'm not accepting pull requests. Open an issue if you find errors or typos. Once the book is stabilized I'll add contribution guidelines and you'll be able to submit a PR. Thanks a lot!**

## Motivation

As much as I love "You don't know JS" and "Eloquent JavaScript" I feel there is the need for a book which takes the reader by hand. Also, everyone has a unique viewpoint on technical topics and my readers love my style of teaching. The "Little JavaScript Book" aims to be a reference on the hard parts of JavaScript aiming at beginners but also at more experienced developers.

## What's in the book?

The "Little JavaScript Book" is organized in three parts. The first part covers the inner working of JavaScript engines and the "hard parts" of the language: closures, the prototype system, this and new. Every chapter ends with a self-assessment test which is useful for making the concepts stick.

The second part of the book has a lot of practical exercises with the DOM with a lot of pages covering code organization and best practices. The third and last part contains solutions for the exercises and future additions to the book.

## What's the difference between free and paid copy of the book?

[Paid](https://leanpub.com/little-javascript/) and [free PDF](https://www.valentinog.com/little-javascript) version of the book have:
 
- **more quizzes** at the end of every chapter
- **solutions for the exercises**
- a lot of **tips and best practices**
 
You can expect these versions to be updated faster and to contain more up to date contents than the free version on Github.

## License & Copyright

The materials herein are all &copy; 2019 Valentino Gagliardi.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivs 4.0 Unported License</a>.
